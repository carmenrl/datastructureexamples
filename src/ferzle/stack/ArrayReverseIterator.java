package ferzle.stack;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * An iterator for the ArrayStack class.
 * 
 * We use a reverse iterator because we want
 * to list the elements in the reverse order
 * for a stack.
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayReverseIterator<T> implements Iterator<T>
{
    T[] theArray;
    int current;
    
    public ArrayReverseIterator(T[] array) {
        theArray=array;
        current=array.length-1;
    }
    public boolean hasNext() {
        return current!=0;
    }
    
    public T next() {
        if(!hasNext()) {
            throw new NoSuchElementException();
        }
        current--;
        return theArray[current];
    }
    
    // We won't implement this operation.  It isn't required.
    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
    
}

