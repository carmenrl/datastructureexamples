package ferzle.queue;

import java.util.Iterator;

/**
 * A simple Queue interface
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */

public interface QueueInterface<T> {
	
	public boolean enqueue(T item);

	public T dequeue();

	public T peek();

	public boolean isEmpty();

	public boolean isFull();
	
	public Iterator<T> iterator();
}
